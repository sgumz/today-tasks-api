<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\TaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


// add v1 prefix to all routes
Route::group([
    'prefix' => 'v1'
], function () {

    // auth routes
    Route::group([
        'prefix' => 'auth',
    ], function () {
        Route::get('guest', [AuthController::class, 'guest']);

    });

    // 'auth:api' middleware group
    Route::group([
        'prefix' => 'auth',
        'middleware' => 'auth:api',
    ], function () {
        Route::get('refresh-token', [AuthController::class, 'refreshToken']);
    });

    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::resource('tasks', TaskController::class)->except(['create', 'edit']);


    });

});
