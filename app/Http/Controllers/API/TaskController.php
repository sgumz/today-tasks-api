<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Priority;
use App\Models\Task;
use Illuminate\Support\Facades\Auth;

class TaskController extends BaseController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // return all tasks paginated by 50
        $tasks = Auth::user()->tasks()->with( 'priority')->paginate(50);
        $success['tasks'] = $tasks;
        $success['priorities'] = Priority::where('active', 1)->get();
        return $this->sendResponse($success, 'Tasks retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreTaskRequest $request)
    {
        // authorize the user's action
        $this->authorize('create', Task::class);

        // create new task and assign it to the authenticated user
        $task = Auth::user()->tasks()->create($request->all());
        $success['task'] = $task->load('priority');
        return $this->sendResponse($success, 'Task created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Task $task)
    {
        // authorize the user's action
        $this->authorize('view', $task);

        // return task
        $success['task'] = $task->load('priority');
        return $this->sendResponse($success, 'Task retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
        // authorize the user's action
        $this->authorize('update', $task);

        // update task
        $task->update($request->all());
        $success['task'] = $task->load('priority');
        return $this->sendResponse($success, 'Task updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Task $task)
    {
        // authorize the user's action
        $this->authorize('delete', $task);

        // delete task
        $task->delete();
        $success['task'] = $task->load('priority');
        return $this->sendResponse($success, 'Task deleted successfully.');
    }
}
