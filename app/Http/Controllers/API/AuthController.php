<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseController
{

    // guest session
    public function guest()
    {
        $user = User::create([
            'name' => 'Guest',
            'is_guest' => true,
        ]);

        Auth::loginUsingId($user->id);

        $success['user_id'] = $user->id;
        $success['token'] = $this->getTokenDetails($user);

        return $this->sendResponse($success, 'Guest session started successfully.');

    }

    // refresh guest session
    public function refreshToken()
    {
        $user = Auth::user();

        $success['user_id'] = $user->id;
        $success['token'] = $this->getTokenDetails($user);

        return $this->sendResponse($success, 'Token refreshed successfully.');
    }

    /**
     * @param User $user
     * @return array
     */
    private function getTokenDetails(User $user): array
    {
        //revoking all tokens
        $user->tokens()->delete();

        //creating new token
        $personal = $user->createToken('Guest #' . $user->id . ' - ' . now());

        return [
            'access' => $personal->accessToken,
            'type' => "Bearer",
            'created_at' => $personal->token->created_at,
            'expires_at' => $personal->token->expires_at,
        ];
    }

}
