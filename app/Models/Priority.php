<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    use HasFactory;

    protected $table = 'priorities';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'level',
        'active',
    ];

    protected $casts = [
        'active' => 'boolean',
    ];

    protected $hidden = [
        'active'
    ];

    public function tasks()
    {
        return $this->belongsToMany(Task::class);
    }


}
