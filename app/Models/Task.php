<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'tasks';

    protected $fillable = [
        'name',
        'priority_id',
        'completed',
        'updated_at',
    ];

    protected $casts = [
        'completed' => 'boolean',
        'priority_id' => 'integer',
    ];

    protected $hidden = [
        'priority_id',
        'user_id',
        'deleted_at',
    ];

    public function priority()
    {
        return $this->hasOne(Priority::class, 'id', 'priority_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
