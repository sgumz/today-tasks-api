<?php

namespace Tests\Feature;

use Tests\TestCase;

class PriorityTest extends TestCase
{

    public string|null $token_access = null;

    //on init, ge the same token as the guest
    public function setUp(): void
    {
        parent::setUp();

        $response = $this->get('api/v1/auth/guest');

        //save access token
        $this->token_access = $response->json()['data']['token']['access'];
    }

    /**
     * A basic feature test example.
     */
    public function test_priorities_are_in_DB(): void
    {

        $priorities = $this->default_properties();

        foreach ($priorities as $priority) {
            $this->assertDatabaseHas('priorities', [
                'name' => $priority,
                'active' => 1,
            ]);
        }

    }


    /**
     * Check if not authenticated user can get all priorities from the database.
     */
    public function test_unauthenticated_user_not_allowed()
    {
        // priorities come at the GET tasks request, with the tasks list
        $response = $this->get('api/v1/tasks', [
            'Accept' => 'application/json',
        ]);

        //check if the response is ok
        $response->assertStatus(401);

        //check json structure
        $response->assertJsonStructure([
            'success',
            'message',
        ]);

        //check if the response contains the correct message
        $response->assertJson([
            'success' => false,
            'message' => 'Unauthenticated.'
        ]);
    }

    /**
     * Check if authenticated user can get all priorities from the database.
     */
    public function test_authenticated_user_can_get_all_priorities()
    {
        $response = $this->get('api/v1/tasks', [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token_access,
        ]);

        //check if the response is ok
        $response->assertStatus(200);

        //check json structure
        $response->assertJsonStructure([
            'success',
            'data' => [
                'priorities' => [
                    '*' => [
                        'id',
                        'name',
                        'level',
                    ],
                ],
            ],
        ]);

        //check if the response contains the correct message
        $response->assertJson([
            'success' => true,
        ]);

        //check if the response contains json data with the correct priorities
        $priorities = $this->default_properties();
        foreach ($priorities as $key => $priority) {
            $response->assertJsonPath("data.priorities.$key.name", $priority);
        }

    }

    /**
     * @return string[]
     */
    private function default_properties(): array
    {
        return ['High', 'Medium', 'Low', 'Very Low'];
    }

}
