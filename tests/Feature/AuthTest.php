<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{

    //private variable to store the token
    public string|null $token_access = null;


    public function test_unauthenticated_user_not_allowed()
    {
        $response = $this->get('api/v1/auth/refresh-token', [
            'Accept' => 'application/json',
        ]);

        //check if the response is ok
        $response->assertStatus(401);

        //check json structure
        $response->assertJsonStructure([
            'success',
            'message',
        ]);

        //check if the response contains the correct message
        $response->assertJson([
            'message' => 'Unauthenticated.'
        ]);
    }

    /**
     * Check if the guest login route is working.
     */
    public function test_starts_a_guest_session()
    {
        $response = $this->get('api/v1/auth/guest');

        //save access token
        $this->token_access = $response->json()['data']['token']['access'];

        //check if the response is ok
        $response->assertStatus(200);

        //check json structure
        $response->assertJsonStructure([
            'success',
            'data' => [
                'user_id',
                'token' => [
                    'access',
                    'type',
                    'created_at',
                    'expires_at'
                ],
            ],
            'message',
        ]);

        //check if the response contains the correct message
        $response->assertJson([
            'message' => 'Guest session started successfully.'
        ]);
    }

    /**
     * Check if the refresh token route is working.
     */
    public function test_refresh_token_session()
    {
        if (!$this->token_access) {
            $this->test_starts_a_guest_session();
        }

        $response = $this->get('api/v1/auth/refresh-token', [
            'Authorization' => 'Bearer ' . $this->token_access,
        ]);

        //check if the response is ok
        $response->assertStatus(200);

        //check json structure
        $response->assertJsonStructure([
            'success',
            'data' => [
                'user_id',
                'token' => [
                        'access',
                        'type',
                        'created_at',
                        'expires_at'
                ],
            ],
            'message',
        ]);

        //check if the response contains the correct message
        $response->assertJson([
            'message' => 'Token refreshed successfully.'
        ]);
    }
}
