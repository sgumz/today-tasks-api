<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Tests\TestCase;

class TaskTest extends TestCase
{

    //private variable to store the token
    private string|null $token_access = null;

    //on init, ge the same token as the guest
    private mixed $task_id = null;

    public function setUp(): void
    {
        parent::setUp();

        $response = $this->get('api/v1/auth/guest');

        //save access token
        $this->token_access = $response->json()['data']['token']['access'];
    }

    /**
     * Check if not authenticated user can get all tasks from the database.
     */
    public function test_unauthenticated_user_not_allowed()
    {
        $response = $this->get('api/v1/tasks', [
            'Accept' => 'application/json',
        ]);

        //check if the response is ok
        $response->assertStatus(401);

        //check json structure
        $response->assertJsonStructure([
            'success',
            'message',
        ]);

        //check if the response contains the correct message
        $response->assertJson([
            'message' => 'Unauthenticated.'
        ]);

        $task = Task::factory()->make();

        //check store method
        $this->post('api/v1/tasks', [
            'name' => $task->name,
            'priority_id' => $task->priority_id,
        ], [
            'Accept' => 'application/json',
        ])->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.'
            ]);

        //check update method
        $this->put('api/v1/tasks/1', [
            'name' => $task->name,
            'priority_id' => $task->priority_id,
        ], [
            'Accept' => 'application/json',
        ])->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.'
            ]);

        //check delete method
        $this->delete('api/v1/tasks/1', [], [
            'Accept' => 'application/json',
        ])->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.'
            ]);
    }


    /**
     * Check if the user can create a task.
     */
    public function test_user_can_create_a_task()
    {
        $task = Task::factory()->make();

        $response = $this->post('api/v1/tasks', [
            'name' => $task->name,
            'priority_id' => $task->priority_id,
        ], [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token_access,
        ]);

        //check if the response is ok
        $response->assertStatus(200);

        //check json structure
        $response->assertJsonStructure([
            'success',
            'data' => [
                'task' => [
                    'id',
                    'name',
                    'priority' => [
                        'id',
                        'name',
                        'level',
                    ],
                    'created_at',
                    'updated_at',
                ],
            ],
        ]);

        //check if the response contains the correct message
        $response->assertJson([
            'message' => 'Task created successfully.',
        ]);

        //save the task id for the next tests
        $this->task_id = $response->json()['data']['task']['id'];

        //check if the task is in the database
        $this->assertDatabaseHas('tasks', [
            'name' => $task->name,
            'priority_id' => $task->priority_id,
        ]);

    }

    /**
     * Check if the user can update a task.
     */
    public function test_user_can_update_a_task()
    {
        $task = Task::factory()->make();

        //check task_id
        if (!$this->task_id) {
            $this->task_id = Task::factory()->create()->id;
        }

        $response = $this->put('api/v1/tasks/'.$this->task_id, [
            'name' => $task->name,
            'priority_id' => $task->priority_id,
            'completed' => $task->completed,
        ], [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token_access,
        ]);

        //check if the response is ok
        $response->assertStatus(200);

        //check json structure
        $response->assertJsonStructure([
            'success',
            'data' => [
                'task' => [
                    'id',
                    'name',
                    'priority' => [
                        'id',
                        'name',
                        'level',
                    ],
                    'completed',
                    'created_at',
                    'updated_at',
                ],
            ],
        ]);

        //check if the response contains the correct message
        $response->assertJson([
            'message' => 'Task updated successfully.',
        ]);

        //check if the task is in the database
        $this->assertDatabaseHas('tasks', [
            'name' => $task->name,
            'priority_id' => $task->priority_id,
            'completed' => $task->completed,
        ]);

    }


    /**
     * Check if the user can get all tasks from the database.
     */

    public function test_user_can_get_all_tasks()
    {
        $response = $this->get('api/v1/tasks', [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token_access,
        ]);

        //check if the response is ok
        $response->assertStatus(200);

        //check json structure
        $response->assertJsonStructure([
            'success',
            'data' => [
                'tasks' => [
                    'data' => [
                        '*' => [
                            'id',
                            'name',
                            'priority' => [
                                'id',
                                'name',
                                'level',
                            ],
                            'completed',
                            'created_at',
                            'updated_at',
                        ],
                    ],
                ],
            ],
        ]);

        //check if the response contains the correct message
        $response->assertJson([
            'message' => 'Tasks retrieved successfully.',
        ]);

        //check pagination structure
        $response->assertJsonStructure([
            'data' => [
                'tasks' => [
                    'current_page',
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'links' => [
                        '*' => [
                            'url',
                            'label',
                            'active',
                        ],
                    ],
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ],
        ]);

    }

    /**
     * Check if the user can get a task from the database.
     */
    public function test_user_can_get_a_task()
    {
        //check task_id
        if (!$this->task_id) {
            $this->task_id = Task::factory()->create()->id;
        }

        $response = $this->get('api/v1/tasks/'.$this->task_id, [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token_access,
        ]);

        //check if the response is ok
        $response->assertStatus(200);

        //check json structure
        $response->assertJsonStructure([
            'success',
            'data' => [
                'task' => [
                    'id',
                    'name',
                    'priority' => [
                        'id',
                        'name',
                        'level',
                    ],
                    'completed',
                    'created_at',
                    'updated_at',
                ],
            ],
        ]);

        //check if the response contains the correct message
        $response->assertJson([
            'message' => 'Task retrieved successfully.',
        ]);

    }

    /**
     * Check if the user can delete a task from the database.
     */
    public function test_user_can_delete_a_task()
    {
        //check task_id
        if (!$this->task_id) {
            $this->task_id = Task::factory()->create()->id;
        }

        $response = $this->delete('api/v1/tasks/'.$this->task_id, [], [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token_access,
        ]);

        //check if the response is ok
        $response->assertStatus(200);

        //check json structure
        $response->assertJsonStructure([
            'success',
            'data' => [
                'task' => [
                    'id',
                    'name',
                    'priority' => [
                        'id',
                        'name',
                        'level',
                    ],
                    'completed',
                    'created_at',
                    'updated_at',
                ],
            ],
        ]);

        //check if the response contains the correct message
        $response->assertJson([
            'message' => 'Task deleted successfully.',
        ]);

        //check if the task is in the database
        $this->assertDatabaseMissing('tasks', [
            [
                'id' => $this->task_id,
            ]
        ]);

    }

    /**
     * Check if the user can access only his tasks.
     */
    public function test_user_can_access_only_his_tasks()
    {
        //create a new user
        $user = User::factory()->create([
            'password' => bcrypt('password')
        ]);

        //create a new task
        $task = $user->tasks()->save(Task::factory()->make());

        $response = $this->get('api/v1/tasks/'.$task->id, [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token_access,
        ]);

        //check if the response is ok
        $response->assertStatus(403);

        //check if the response contains the correct message
        $response->assertJson([
            'success' => false,
            'message' => 'You do not own this task.',
        ]);


    }

}
