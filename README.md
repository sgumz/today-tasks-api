# Today's Tasks API Documentation

Welcome to the API documentation for Today's Tasks, a project built in Laravel that provides endpoints to manage tasks. This document outlines the available endpoints, their functionalities, and the required parameters based in the following app design:

[//]: # (Image References: app-screenshot.jpeg)

![App Screenshot](app-screenshot.jpeg)

## Installation and Setup

To install the project, clone the repository and run the following commands:

```bash
composer install
cp .env.example .env
touch database/database.sqlite
php artisan key:generate
php artisan passport:install
php artisan migrate --seed
```

## Running tests and generating code coverage

To run the tests, run the following command:

```bash
php artisan test
```

## API Endpoints at Postman

The API endpoints are available at Postman for testing purposes. 

[![Run in Postman](https://run.pstmn.io/button.svg)](https://www.postman.com/galactic-crater-215522/workspace/today-s-tasks-sandbox)

## Authentication

- `GET /api/v1/auth/guest`: Start a new guest session for authentication.
- `GET /api/v1/auth/refresh-token`: Refresh the current access token.

A Bearer Token is generated for each guest session and is required to access the API. The token is valid for 3 months and can be refreshed using the `/api/v1/auth/refresh-token` endpoint.

## Task Endpoints

### Create/Retrieve/Update/Delete Tasks

- `GET /api/v1/tasks`: Retrieve a list of tasks.
- `POST /api/v1/tasks`: Create a new task.
- `GET /api/v1/tasks/{id}`: Retrieve a single task.
- `PUT /api/v1/tasks/{id}`: Update a task.
- `DELETE /api/v1/tasks/{id}`: Delete a task.

### Task Properties

For each task, the following properties are required:

- `name`: The name of the task.
- `priority_id`: Priority level of the task (High, Medium, Low, Very Low).
- `completed`: Whether the task is completed or not.

### Priorities Properties

All tasks are assigned a priority level. The priority levels are listed in the same endpoint as the list of tasks (`GET /api/v1/tasks`) to avoid additional requests.

### Task Completion

To mark a task as completed, trigger a PUT request with the property `completed` set to `true` to the following endpoint:

- `PUT /api/v1/tasks/{id}`

## Task Pagination

Tasks are returned in pages of 50 tasks per page. To retrieve a specific page, use the `page` query parameter:

- `GET /api/v1/tasks?page=2`

---

Please note that this documentation provides an overview of the available endpoints and their functionalities. For detailed usage instructions and examples, refer to the actual API implementation or accompanying code documentation.

For any further assistance or inquiries, feel free to contact me at [sgumz@me.com](mailto:sgumz@me.com) or visit my [website](https://sgumz.com).
