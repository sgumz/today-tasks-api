<?php

namespace Database\Seeders;

use App\Models\Priority;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PrioritySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        // default priorities
        $priorities = [
            [
                'name' => 'High',
                'level' => 1,
                'active' => true,
            ],
            [
                'name' => 'Medium',
                'level' => 1,
                'active' => true,
            ],
            [
                'name' => 'Low',
                'level' => 3,
                'active' => true,
            ],
            [
                'name' => 'Very Low',
                'level' => 4,
                'active' => true,
            ],
        ];

        foreach ($priorities as $priority) {
            Priority::create($priority);
        }

    }
}
