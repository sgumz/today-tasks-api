<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Auth;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Task>
 */
class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => substr($this->faker->sentence(3), 0, 32),
            'priority_id' => $this->faker->numberBetween(1, 4),
            'completed' => $this->faker->boolean(50),
            'user_id' => Auth::id(),
        ];
    }
}
