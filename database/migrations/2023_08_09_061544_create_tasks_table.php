<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('name', 64);
            $table->foreignId('priority_id')->nullable()->constrained('priorities')->onDelete('set null');
            $table->foreignId('user_id')->nullable(false)->constrained('users')->onDelete('cascade');
            $table->boolean('completed')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropForeign(['priority_id', 'user_id']);
        });

        Schema::dropIfExists('tasks');
    }
};
